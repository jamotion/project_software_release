# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo on 17.07.18.
#
# imports of python lib
import logging

# imports of openerp
from datetime import datetime

from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class ProjectIssueVersionPartner(models.Model):
    # Private attributes
    _name = 'project.issue.version.partner'
    _description = 'Versions deployed to Partner'
    _order = 'date DESC'
    
    # Default methods
    
    # Fields declaration
    partner_id = fields.Many2one(
            comodel_name="res.partner",
            string="Partner",
            ondelete="cascade",
    )
    version_id = fields.Many2one(
            comodel_name="project.issue.version",
            string="Version",
            ondelete="cascade",
    )
    date = fields.Date(
            string="Date",
            default=datetime.today(),
    )
    # compute and search fields, in the same order that fields declaration
    
    # Constraints and onchanges
    
    # CRUD methods
    
    # Action methods
    
    # Business methods
