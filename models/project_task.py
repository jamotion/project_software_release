# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo on 22.05.18.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class ProjectTask(models.Model):
    # Private attributes
    _inherit = 'project.task'
    
    # Default methods
    
    # Fields declaration
    version_id = fields.Many2one(
            comodel_name="project.issue.version",
            string="Version",
            help="The Changes are deployed in given version.",
    )
    
    public_description = fields.Text(
        string="Public description",
    )
    # compute and search fields, in the same order that fields declaration
    
    # Constraints and onchanges
    
    # CRUD methods
    
    # Action methods
    
    # Business methods
