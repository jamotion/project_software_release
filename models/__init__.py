# -*- coding: utf-8 -*-
from . import project_task
from . import project_task_type
from . import project_issue
from . import project_issue_version
from . import res_partner
from . import project_issue_version_partner
