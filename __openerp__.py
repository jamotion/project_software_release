# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Renzo on 2018-05-22.
#
{
    'name': 'Software Release Management',
    'version': '8.0.1.0.0',
    'category': 'Project Management',
    'author': 'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Manage Software Releases based on Project Tasks',
    'images': [],
    'depends': [
        'tko_project_task_type',
        'project_issue',
        'project_issue_task',
        'project_optimizations',
        'project_task_relations',
        'website',
    ],
    'data': [
        'views/project_issue_view.xml',
        'views/project_task_view.xml',
        'views/res_partner_view.xml',
        'wizards/project_issue_version_deploy_wizard_view.xml',
        'data/project_menu.xml',
        'views/templates.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'test': [],
    'application': False,
    'active': False,
    'installable': True,
}

