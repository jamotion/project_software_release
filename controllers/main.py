# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo on 23.05.18.
#
import logging

from openerp import http
from openerp.http import request


class Main(http.Controller):
    
    @http.route(['/changelogs'],
                type='http', auth="public", website=True)
    def changelogs(self, **kw):
        env = request.env
        versions = env['project.issue.version'].sudo().search(
                [('website_published', '=', True)], order='date DESC')
        values = {
            'versions': versions,
        }
        return request.website.render(
                "project_software_release.changelogs",
                values)
    
    @http.route(['/changelog/<model("project.issue.version"):version>'],
                type='http', auth="public", website=True)
    def changelog_version(self, version, **kw):
        values = {
            'version': version,
            'projects': version.task_ids.mapped('project_id'),
        }
        
        return request.website.render(
                "project_software_release.changelog",
                values)
    
    @http.route([
        '/changelog/project/<int:project_id>',
        '/changelog/project/<model("project.project"):project>',
    ],
            type='http', auth="public", website=True)
    def changelog_project(self, project_id=False, project=False, **kw):
        if not project_id:
            project_id = project.id
        project = request.env['project.project'].sudo().browse(project_id)
        versions = request.env['project.task'].sudo().search([
            ('version_id', '!=', False),
            ('project_id', '=', project.id),
        ]).mapped('version_id')
        versions = versions.sorted(key=lambda s: s.date, reverse=True)
        values = {
            'versions': versions,
            'project': project
        }
        
        return request.website.render(
                "project_software_release.changelog_project",
                values)
