# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by renzo on 22.05.18.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
from . import namegenerator

_logger = logging.getLogger(__name__)


class ProjectIssueVersionDeployWizard(models.TransientModel):
    # Private attributes
    _name = 'project.issue.version.deploy.wizard'
    
    # Default methods
    @api.model
    def default_get(self, f):
        version_name = self._generate_name()
        tasks = self.env['project.task'].search([
            ('stage_id.use_deploy', '=', True),
            ('version_id', '=', False),
        ], order='project_id,task_type_id,id')
        
        stage = self.env['project.task.type'].search([
            ('use_done', '=', True),
        ], limit=1)
        latest_version = self.env['project.issue.version'].search(
                [], order='date DESC, number DESC', limit=1)
        
        return {
            'version_name': version_name.title(),
            'version_number': '8.0.x.y.z',
            'latest_version_number': latest_version and
                                     latest_version.number or False,
            'task_ids': tasks.ids,
            'version_intro': _(u'Some words about this version'),
            'new_version_type': 'manual',
            'date': fields.Date.today(),
            'website_published': True,
            'stage_id': stage.id,
        }
    
    def _generate_name(self):
        version_name = namegenerator.get_random_name()
        versions = self.env['project.issue.version'].search([])
        while versions.filtered(lambda f: f.name == version_name):
            version_name = namegenerator.get_random_name()
        return version_name
    
    # Fields declaration
    version_number = fields.Char(
            string="Version Number",
            required=True,
    )
    latest_version_number = fields.Char(
            string="Last Number",
            readonly=True,
    )
    version_name = fields.Char(
            string="Version Name",
            required=True,
    )
    version_intro = fields.Html(
            string="Version Intro",
            required=True,
    )
    new_version_type = fields.Selection(
            selection=[
                ('major', 'Major'),
                ('minor', 'Minor'),
                ('revision', 'Revision'),
                ('manual', 'Manual'),
            ],
            string="Release Type",
    )
    date = fields.Date(
            string="Date",
            required=True,
    )
    website_published = fields.Boolean(
            string="Published",
    )
    task_ids = fields.Many2many(
            comodel_name="project.task",
            string="Tasks",
    )
    stage_id = fields.Many2one(
            comodel_name="project.task.type",
            string="New Task Stage",
            required=True,
    )
    
    # compute and search fields, in the same order that fields declaration
    
    # Constraints and onchanges
    @api.onchange('new_version_type')
    def _onchange_new_version_type(self):
        if self.new_version_type == 'manual':
            return
        try:
            version_parts = self.latest_version_number.split('.')
            if self.new_version_type == 'major':
                key = 2
            elif self.new_version_type == 'minor':
                key = 3
            else:
                key = 4
            
            version_parts[key] = str(int(version_parts[key]) + 1)
            if key < 4:
                for k in range(key + 1, 5):
                    version_parts[k] = '0'
            
            self.version_number = '.'.join(version_parts)
        except:
            return
    
    # CRUD methods
    
    # Action methods
    @api.multi
    def create_release(self):
        self.ensure_one()
        
        new_version = self.env['project.issue.version'].create({
            'number': self.version_number,
            'name': self.version_name,
            'intro': self.version_intro,
            'date': self.date,
            'website_published': self.website_published,
        })
        
        self.task_ids.write({
            'version_id': new_version.id,
            'stage_id': self.stage_id.id,
        })
        
        issues = self.task_ids.mapped('issue_id').filtered(
                lambda f: not f.resolved_version_id)
        if issues:
            issues.write({
                'resolved_version_id': new_version.id,
                'stage_id': self.stage_id.id,
            })
    
    @api.multi
    def generate_name(self):
        self.ensure_one()
        self.version_name = self._generate_name()
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'project.issue.version.deploy.wizard',
            'res_id': self.id,
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }
    
    # Business methods
